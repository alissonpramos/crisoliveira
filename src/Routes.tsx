import React from 'react';

import { Route, BrowserRouter } from "react-router-dom";
import Home from './assets/Pages/Home';
import KnowMore from './assets/Pages/KnowMore';
import Services from './assets/Pages/Services';
import Sayings from './assets/Pages/Sayings';
import Budget from './assets/Pages/Budget';

function Routes() {
    return(
        <BrowserRouter>
            <Route path="/orcamento" component={Budget} exact/> 
            <Route path="/servicos" component={Services} exact/> 
            <Route path="/depoimentos" component={Sayings} exact/> 
            <Route path="/saiba-mais" component={KnowMore} exact/> 
            <Route path="/" component={Home} exact/>
        </BrowserRouter>
    )
}

export default Routes;