import React from 'react';

import Home from './assets/Pages/Home';

import './global.css'
import Routes from './Routes';

function App() {
  return (
    <div className="App">
      <Routes/>
    </div>
  );
}

export default App;
