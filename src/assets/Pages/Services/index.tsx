import React from "react";

import Footer from "../../components/Footer";
import MainMenu from "../../components/Menu";

import { Link } from "react-router-dom";

import './styles.css';

function Services() {
    return(
        <div>
            <MainMenu/>

            <section className="services">
                <div className="title">
                    <h1 className="side-kick">Nossos</h1>
                    <h1 className="highlight">
                        Serviços
                    </h1>
                </div>

                <p className="text">
                    Nós trabalhamos com três principais tipos de assessoria:
                </p>
            </section>

            <section className="service-complete">
                <div className="content">
                    <h1 className="title">
                        Assessoria Completa
                    </h1>

                    <p className="text">
                        É destinada aos clientes que desejam um acompanhamento bem de perto e/ou não tem muito norte sobre a organização do seu evento, contando com nossa assessoria em cada detalhe. <br/> <br/>

                        Na assessoria completa fazemos todo o acompanhamento na contratação de todos os fornecedores, começando com as indicações, depois pedidos de orçamento, em seguida reuniões e visita juntamente com o cliente aos fornecedores, análise técnica dos contratos e fechamento. Tudo dentro do perfil de cada evento.

                    </p>

                    <div className="button">
                        <Link to="/orcamento" className="label">
                            Faça o orçamento
                        </Link>                                        
                    </div>
                </div>
            </section>

            <section className="service-medium">
                <div className="content">
                    <h1 className="title">
                        Assessoria Intermediária
                    </h1>

                    <p className="text">
                        É destinada aos clientes que desejam um acompanhamento de perto, mas que tem certa autonomia, ou que já contrataram alguns fornecedores optando então pelo acompanhamento bem de perto para alguns fornecedores específicos. <br/> <br/>

                        Na assessoria completa fazemos todo o acompanhamento na contratação de 5(cinco) fornecedores, começando com as indicações, depois pedidos de orçamento, em seguida reuniões e visita juntamente com o cliente aos fornecedores, análise técnica dos contratos e fechamento. Tudo dentro do perfil de cada evento.

                    </p>

                    <div className="button">
                        <Link to="/orcamento" className="label">
                            Faça o orçamento
                        </Link>                                        
                    </div>
                </div>
            </section>

            <section className="service-final">
                <div className="content">
                    <h1 className="title">
                        Assessoria Final
                    </h1>

                    <p className="text">
                        A assessoria final conhecia também como cerimonial dia, é destinada aos clientes que se preocupam com a organização do seu evento no dia em que ele irá acontecer. Esses clientes já contrataram a maioria dos fornecedores e/ou possuem referência sobre os mesmos, e assim não precisam de uma assessoria tão de perto. <br/> <br/>

                        Na assessoria final nós tiramos todas as dúvidas que possam surgir durante a preparação do evento, assim como nas indicações dos fornecedores não contratados

                    </p>

                    <div className="button">
                        <Link to="/orcamento" className="label">
                            Faça o orçamento
                        </Link>                                        
                    </div>
                </div>
            </section>

            <Footer/>
        </div>
    )
}

export default Services;