import React from "react";

import Footer from '../../components/Footer'
import ExampleCompleteCard from "../../components/ExampleCompleteCard";
import FotoExemplo from '../../img/bannerPrincipal.jpg';
import MainMenu from "../../components/Menu";

import { Link } from "react-router-dom";

import './styles.css';

function Sayings() {
    return(
        <div>

            <MainMenu/>

            <section className="examples cards">
                    <div className="title">
                        <h1 className="side-kick">
                            O que nossos <br/>
                            <b className="highlight">clientes</b> falam <br/>
                            sobre nós?
                        </h1>
                    </div>

                    <p className="text">
                        Saiba o que diz quem já <br/>
                        utilizou nossos serviços. 
                    </p>

                    <ExampleCompleteCard
                        title='"Equipe Maravilhosa"'
                        img={FotoExemplo}
                        text='Lorem Lorem Lorem Lorem LoremL oremL oremLor emL ore mLore Lor em'
                        signature='Luciana Verdolim'
                    />

                    <ExampleCompleteCard
                        title='"Equipe Maravilhosa"'
                        img={FotoExemplo}
                        text='Lorem Lorem Lorem Lorem LoremL oremL oremLor emL ore mLore Lor em'
                        signature='Luciana Verdolim'
                    />

                    <ExampleCompleteCard
                        title='"Equipe Maravilhosa"'
                        img={FotoExemplo}
                        text='Lorem Lorem Lorem Lorem LoremL oremL oremLor emL ore mLore Lor em'
                        signature='Luciana Verdolim'
                    />
                </section>

                <section className="budget, sayings-budget">
                        <div className="title">
                            <h1 className="side-kick">
                                Orçamento em 1 minuto
                            </h1>
                        </div>

                        <p className="text">
                            Olá! Por favor preencha as informações para que possamos gerar o seu orçamento. Leva menos de 1 minuto!
                        </p>

                        <div className="button">
                            <Link to="/orcamento" className="label">
                                Clique aqui
                            </Link>                                        
                        </div>
                </section>

                <Footer/>
 
        </div>
    )
}

export default Sayings;