import React from 'react';

import { Link } from "react-router-dom";

import ProcessItem from '../../components/Process-item';
import Footer from '../../components/Footer'
import Carousel from '../../components/Carousel';
import Photo1 from '../../img/photo1a.png';
import Photo2 from '../../img/photo2a.png';
import Photo3 from '../../img/photo3a.png';
import Photo4 from '../../img/photo4.png';
import Photo5 from '../../img/photo5.png';
import Photo6 from '../../img/photo6.png';
import MainMenu from '../../components/Menu';
import Card from '../../components/Card';

import './styles.css';
import CardCarousel from '../../components/CardCarousel';

function Home() {
    return(

        <div>
            <MainMenu/>

            <div className="content">
                <section className="main-banner">
                    <h2>
                        Somos especialistas <br/>
                        em realizar <br/>
                        sonhos.
                    </h2>

                    <div className = "material-icons">
                        <span className = "material-icon">
                            keyboard_arrow_down
                        </span>
                    </div>

                    
                </section>
                
                <section className="about-me">            
                    <div className="title">
                        <h1 className="side-kick">Olá!</h1>
                        <h1 className="highlight">
                            Sou a Cris <br/>
                            Oliveira.
                        </h1>
                    </div>

                    <p className="text">
                        Tenho muitos sonhos realizados por Deus, dentre eles meu casamento e uma família linda. <br/>
                        (formada em matemática e especialista em realizar sonhos)
                    </p>

                    <Carousel
                        content1={Photo1}
                        content2={Photo2}
                        content3={Photo3}
                    />

                    <div className="icons">
                        <div className="icons-unit">
                            <div className="icon-container">
                                <span className = "material-icons">
                                    favorite_border
                                </span>
                            </div>
                            <h3>
                                Espiritualidade <br/>
                                harmônica
                            </h3>
                        </div>

                        <div className="icons-unit">
                            <div className="icon-container">
                                <span className = "material-icons">
                                    emoji_emotions
                                </span>
                            </div>
                            <h3>
                                Vivência e <br/>
                                experiência
                            </h3>
                        </div>

                        <div className="icons-unit">
                            <div className="icon-container">
                                <span className = "material-icons">
                                    sentiment_satisfied_alt
                                </span>
                            </div>
                            <h3>
                                Emocionalmente <br/>
                                preparada
                            </h3>
                        </div>
                    </div>

                    <div className="button">
                        <Link to="/saiba-mais" className="label">
                            Saiba Mais
                        </Link>                            
                    </div>
                </section>

                <section className="about-us">
                    <div className="title">
                        <h1 className="side-kick">
                            Somos uma <br/>
                            empresa de <br/>
                            Assessoria e <br/>
                            Cerimonial.    
                        </h1>
                        <h1 className="highlight">
                            Mas o que <br/>
                            seria isso?
                        </h1>
                    </div>

                    <p className="text">
                        Nós damos toda a assessoria que os noivos precisam na preparação do seu evento. Trabalhamos com três tipos de Assessoria, Completa, Parcial e Final, fazendo um acompanhamento de perto em tudo que envolve a contratação dos fornecedores, inclusive na assessoria final. O que nos faz especialistas em realizar sonhos.
                    </p>

                    <Carousel
                        content1={Photo4}
                        content2={Photo5}
                        content3={Photo6}
                    />

                    <div className="button">
                        <Link to="/saiba-mais" className="label">
                            Saiba Mais
                        </Link>                                         
                    </div>
                </section>

                <section className="examples">
                    <div className="title">
                        <h1 className="side-kick">
                            O que nossos <br/>
                            <b className="highlight">clientes</b> falam <br/>
                            sobre nós?
                        </h1>
                    </div>

                    <p className="text">
                        Saiba o que diz quem já <br/>
                        utilizou nossos serviços. 
                    </p>

                    <CardCarousel
                        content1={<Card
                            name="Luciana Verdolim"
                            quote="Equipe Maravilhosa"
                            img={Photo1}
                        />}    
                        content2={<Card
                            name="Luciana Verdolim"
                            quote="Equipe Maravilhosa"
                            img={Photo1}
                        />}    
                        content3={<Card
                            name="Luciana Verdolim"
                            quote="Equipe Maravilhosa"
                            img={Photo1}
                        />}    
                    />

                    <div className="button">
                        <Link to="/depoimentos" className="label">
                            Depoimentos
                        </Link>                                           
                    </div>
                </section>

                <section className="process">
                    <div className="title">
                        <h1 className="side-kick">
                            Veja como <br/>
                            funciona
                        </h1>
                    </div>

                    <p className="text">
                        Entenda como funciona todo o <br/>
                        processo até o fim do seu <br/>
                        evento.
                    </p>

                    <div className="process-list">
                        
                        <ProcessItem 
                            icon="archive"
                            title="Orçamento"
                            text="Selecione os serviços desejados e logo você receberá sua cotação por email"
                        />

                        <ProcessItem 
                            icon="archive"
                            title="Orçamento"
                            text="Selecione os serviços desejados e logo você receberá sua cotação por email"
                        />

                        <ProcessItem 
                            icon="archive"
                            title="Orçamento"
                            text="Selecione os serviços desejados e logo você receberá sua cotação por email"
                        />

                        <ProcessItem 
                            icon="archive"
                            title="Orçamento"
                            text="Selecione os serviços desejados e logo você receberá sua cotação por email"
                        />

                    </div>
                </section>

                <section className="budget">
                    <div className="budget-card">
                        <div className="title">
                            <h1 className="side-kick">
                                Orçamento em 1 minuto
                            </h1>
                        </div>

                        <p className="text">
                            Olá! Por favor preencha as informações para que possamos gerar o seu orçamento. Leva menos de 1 minuto!
                        </p>

                        <div className="button">
                            <Link to="/orcamento" className="label">
                                Clique aqui
                            </Link>                                        
                        </div>
                    </div>
                </section>

                <section className="form">
                    <div className="title">
                        <h1 className="side-kick">
                            Fale <br/> conosco!
                        </h1>
                    </div>
                    <form action="">
                        <label htmlFor="name">Nome Completo</label><br/>
                        <input type="text" id="name" name="name" /><br/>

                        <label htmlFor="email">Email</label><br/>
                        <input type="email" id="email" name="email"/><br/>

                        <label htmlFor="number">Telefone</label><br/>
                        <input type="tel" id="number" name="number"/><br/>

                        <label htmlFor="message">Mensagem</label><br/>
                        <textarea name="message" id="message"></textarea>

                        <input className="button-submit" type="submit"/>
                    </form>

                </section>

                <Footer/>
            </div>
        </div>
    );
}

export default Home;
