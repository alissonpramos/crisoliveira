import React from "react";

import { Link } from "react-router-dom";

import Footer from "../../components/Footer";
import bannerImg from "../../img/bannerPrincipal.jpg";
import MainMenu from '../../components/Menu';

import Photo1 from '../../img/photo1a.png';
import Photo2 from '../../img/photo2a.png';
import Photo3 from '../../img/photo3a.png';

import './styles.css';
import CarouselWorking from "../../components/Carousel";

function KnowMore() {
    return(
        <div className="knowMore">
            <MainMenu/>

            <section className="about-me">
                <div className="content">
                    <div className="title">
                            <h1 className="side-kick">Quem sou?</h1>
                            <h1 className="highlight">
                                Sou a Cris <br/>
                                Oliveira.
                            </h1>
                        </div>

                        <p className="text">
                            Tenho muitos sonhos realizados por Deus, dentre eles meu casamento e uma família linda. <br/><br/>
                            Sou formada em matemática e pós-graduada no ensino de matemática com o uso de tecnologias, e embora pareçam áreas totalmente diferentes, ambas trabalham com pessoas e lidam com sonhos. <br/><br/>
                            Esse mix de vivência versos experiência, me tornam uma pessoa emocionalmente preparada e também qualificada para gerir os processos organizacionais da preparação de cada evento, entender e cuidar de todos os sentimentos que palpitam o coração na caminhada chama preparativos, e como plus uma pessoa com espiritualidade harmônica. <br/><br/>
                            Deste modo seja qual for o seu sonho, casamento, 15 anos ou bodas, ele será realizado.
                        </p>
                </div>            

                </section>

                <img className="main-img" src= {bannerImg} alt="Cris Oliveira Cerimonial"/>

                <section className="about-us">
                    <div className="title">
                        <h1 className="side-kick">
                            Quem somos?
                        </h1>
                        <h1 className="highlight">
                            Somos uma <br/>
                            empresa de <br/>
                            Assessoria e <br/>
                            Cerimonial.    
                        </h1>
                    </div>

                    <p className="text">
                        Assessoria é todo o acompanhamento feito aos clientes antes do evento. <br/><br/> Na fase dos preparativos do evento ajudamos cada cliente com indicação de fornecedores, pedidos de orçamento, visita com os clientes aos fornecedores, análise técnica dos contratos, dentre outros e tudo dentro do perfil de cada cliente e evento. Lembramos que a escolha permanece sendo do cliente, nós apenas orientamos e acompanhamos para que esta escolha seja assertiva.<br/><br/>
                        Cerimonial é a parte da organização dos protocolos do evento no dia em que este acontece. <br/><br/> Iniciamos com a recepção dos fornecedores para a montagem do evento, conferência dos itens contratados, recepção dos convidados, direcionamento de cada passo do evento e finalização com a conferência final.
                    </p>

                    <CarouselWorking
                        content1={Photo1}
                        content2={Photo2}
                        content3={Photo3}
                    />

                </section>

                <section className="budget-about">
                    
                    <div className="title">
                        <h1 className="side-kick">
                            Orçamento em 1 minuto
                        </h1>
                    </div>

                    <p className="text">
                        Olá! Por favor preencha as informações para que possamos gerar o seu orçamento. Leva menos de 1 minuto!
                    </p>

                    <div className="button">
                        <Link to="/orcamento" className="label">
                            Clique aqui
                        </Link>                                       
                    </div>
                    
                </section>

                <Footer/>
        </div>
    )
}

export default KnowMore;