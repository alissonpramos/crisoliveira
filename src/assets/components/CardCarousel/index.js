import React from 'react';

import InfiniteCarousel from 'react-leaf-carousel';

import './styles.css';

function CardCarousel (props) {
    return (  
        <div className="carousel">
            <InfiniteCarousel
                dots={false}
                showSides={true}
                sidesOpacity={1}
                sideSize={.07}
                slidesToScroll={1}
                slidesToShow={1}
                scrollOnDevice={false}
            >
                {props.content1}
                {props.content2}
                {props.content3}
            </InfiniteCarousel>
        </div>

    )
}

export default CardCarousel;