import React from 'react';

import './styles.css'

interface card {
    name: string,
    quote: string,
    img: string;
}

function Card({name, quote, img}: card) {
    return(
        <div className="card-border">
            <h2 className="title">{name}</h2>
            <h2 className="quote">{quote}</h2>
            <img src={img} alt="Foto"/>
        </div>
    )
}

export default Card;