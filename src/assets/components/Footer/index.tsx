import React from 'react';

import facebookIcon from '../../img/icons/facebook.png'
import instagramIcon from '../../img/icons/instagram.png'
import logoIcon from '../../img/icons/logo_curta.png'


import './styles.css'

function Footer() {
    return(
        <div>
            <section className="footer">
                <div className="info">
                    <div className="icon-container">
                        <span className = "material-icons">
                            call
                        </span>
                    </div>

                    <p className="text">
                        Ligue para a gente <br/>
                        (31) 9999-9999
                    </p>

                    <div className="icon-container">
                        <span className = "material-icons">
                            query_builder
                        </span>
                    </div>

                    <p className="text, time">
                        Horário de atendimento
                    </p>

                    <p className="sub-text">
                        8:30 hrs às 18:00 hrs, de segunda a sexta
                    </p>

                    <p className="text">
                        Nossas <br/>
                        Redes Sociais
                    </p>

                    <div className="socials">
                        <div className="icon-container">
                            <img src= {facebookIcon} alt="Facebook"/>
                        </div>
                        
                        <div className="icon-container">
                        <img src= {instagramIcon} alt="Instagram"/>
                        </div>
                    </div>
                </div>

                <img className="logo-icon" src= {logoIcon} alt="Cris Oliveira Cerimonial"/>
            </section>
        </div>
    )
}

export default Footer;