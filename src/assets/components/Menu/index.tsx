import React from 'react';

import mainLogo from '../../img/icons/logo_p.png'

import {push as Menu} from 'react-burger-menu';
import {Link} from 'react-router-dom';
import './styles.css';

function MainMenu() {
    return ( 
        <div className="menu-container">

            <Link to="/">
                <img src= {mainLogo} alt="Logo Cris Oliviera Cerimonial"/>
            </Link>

            <div className="bm-burger-button">
                {/* <span className = "material-icon">
                    keyboard_arrow_down
                </span>

                    <button >Open Menu</button> */}
            </div>

            <Menu
                right 
                className="my-menu"
                id="sidebar"
                width={ '50vw' }
            >

                <Link className="menu-item" to="/saiba-mais">
                        <span className = "material-icons" id="item-icon">
                            groups
                        </span>
                        <p className="item-text">
                            Quem Somos
                        </p>              
                </Link>

                <Link className="menu-item" to="/depoimentos">
                    <span className = "material-icons" id="item-icon">
                        record_voice_over
                    </span>
                    <p className="item-text">
                    Depoimentos
                    </p>
                </Link>

                <Link className="menu-item" to="/servicos">
                    <span className = "material-icons" id="item-icon">
                        cake
                    </span>
                    <p className="item-text">
                        Serviços
                    </p>
                </Link>

                <Link className="menu-item" to="/orcamento">
                    <span className = "material-icons" id="item-icon">
                        payments
                    </span>
                    <p className="item-text">
                        Orçamento
                    </p>
                </Link>

            </Menu>

        </div>
    )
}

export default MainMenu;