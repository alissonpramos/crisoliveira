import React, { Component } from 'react';

import './styles.css';

interface card {
    title: string;
    img: string;
    text: string;
    signature: string
}

function ExampleCompleteCard({title, img, text, signature}: card) {
    return(
        <div>
            <div className="card">
                <h1 className="title">{title}</h1>
                <img src={img} alt=""/>
                <p className="text">{text}</p>
                <h2 className="signature">{signature}</h2>
            </div>
        </div>
    )
}

export default ExampleCompleteCard;