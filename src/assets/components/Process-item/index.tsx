import React from 'react';

import './styles.css';

interface processItem {
    icon: string;
    title: string;
    text: string;
}

function ProcessItem({icon, title, text}: processItem) {
    return(
        <div className="process-item">
            <span className = "material-icons">
                {icon}
            </span>

            <p className="item-title">{title}</p>
            <p className="item-text">{text}</p>
        </div>
    )
}

export default ProcessItem;

