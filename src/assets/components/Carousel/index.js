import React from 'react';

import InfiniteCarousel from 'react-leaf-carousel';

import './styles.css';

function CarouselWorking (props) {
    return (  
        <div className="carousel">
            <InfiniteCarousel
                dots={false}
                showSides={true}
                sidesOpacity={1}
                sideSize={.07}
                slidesToScroll={1}
                slidesToShow={1}
                scrollOnDevice={false}
            >
                <img className="carousel-item" src={props.content1} alt="Fotos"/>
                <img className="carousel-item" src={props.content2} alt="Fotos"/>
                <img className="carousel-item" src={props.content3} alt="Fotos"/>
                {props.content4}
            </InfiniteCarousel>
        </div>

    )
}

export default CarouselWorking;